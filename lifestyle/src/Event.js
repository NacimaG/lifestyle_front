import React, { Component } from 'react';
import Comment from './Comment'
import Parts from './img/participant.png'
import './css/my_style.css';
import comments from './img/comments.png'
import { UncontrolledCollapse, Button, CardBody, Card } from 'reactstrap';
/**
 * Composant évènement qui décrit un évènement qu'on peut rejoindre et/ou commenter
 * @author Shokufeh AHMADI SIMABE -- Nassima GHOUT
 */
import axios from 'axios'
import './connexion.css'
class Event extends Component {
        constructor(props){
            super(props)
            this.state={nbParticipants:"0", content: null , user:null, commentslist:[] ,numPart: Parts , commentPIC:comments,
                        com:"0"}
            this.handleDeleteEvent = this.handleDeleteEvent.bind(this)
            this.handleJoinEvent = this.handleJoinEvent.bind(this) 
            this.handleComment= this.handleComment.bind(this)
            this.Participant();
            this.Participant = this.Participant.bind(this) 
            this.handleInputComment=this.handleInputComment.bind(this)
            this.getAllComments();
            this.getAllComments =this.getAllComments.bind(this);

        }
/**
 * récupérer le nombre de personnes participants à l'évènement
 */        
Participant(){
    var code
    var url= new URLSearchParams();
    url.append('idEvent',this.props.event_id)
    axios.get('http://localhost:8080/Life_Style/CountPartEvents?'+url)
			.then(response => {
                code=response.data["code"];
				if(code === 200){
                    this.setState({nbParticipants:response.data['nbParticipants']})
                }
				else if(code === -10){
					alert("User not connected false in cle" , this.props.cle)
				}
				else if(code === 1){
					alert("User not connected")
				}
			})  
} 
/**
 * suppression d'un évènement
 * 
 */        
handleDeleteEvent() {
    var code
    var url= new URLSearchParams();
    url.append('key', this.props.cle)
    url.append('id',this.props.id)
    axios.get('http://localhost:8080/Life_Style/DeleteEvent?'+url)
			.then(response => {
                code=response.data["code"];
                alert(code)
				if(code === 200){
                    alert("Delete Successful")
                    //window.location.reload(false);
                    //this.setState(Friend)
                }
				else if(code === -1){
					alert("Pas d'arguments")
				}
				else if(code === -10){
					alert("User not connected")
				}
			})   
}   
/**
 * appel du servlet pour rejoindre un évènement
 */
handleJoinEvent(){
    var code
    var url= new URLSearchParams(); 
    url.append('name',this.props.name)
    url.append('owner',this.props.owner)
    url.append('key', this.props.cle)
    axios.get('http://localhost:8080/Life_Style/JoinEvent?'+url)
			.then(response => {
                code=response.data["code"];
                //alert(code)
				if(code === 200){
                    alert("join successful")
                    //window.location.reload(false);
                    //this.setState(Friend)
                }
				else if(code === -10){
					alert("User not connected false in cle" , this.props.cle)
				}
				else if(code === 1){
					alert("Already joint")
				}
			})
} 
/**
 * Commenter un évènement
 */

handleComment(){
    var code
    var url= new URLSearchParams(); 
    url.append('key', this.props.cle)
    url.append('idEvent',this.props.event_id)
    url.append('content', this.state.content)
    axios.get('http://localhost:8080/Life_Style/NewComment?'+url)
			.then(response => {
               // console.log(response.data)
                code=response.data["code"];
                //alert(code)
				if(code === 200){
                   // alert("Comment successful")
                    this.getAllComments()
                    this.setState({content:"  "})
                    //window.location.reload(false);
                    //this.setState(Friend)
                }
				else if(code === -10){
					alert("User not connected false in cle" , this.props.cle)
				}
				else if(code === 1){
					alert("User not connected")
				}
			})
} 

handleInputComment= (e) =>{
    this.setState({content : e.target.value})
  }

getAllComments(){
    var url= new URLSearchParams(); 
    url.append('idEvent',this.props.event_id)
    axios.get('http://localhost:8080/Life_Style/ListComments?'+url)
    .then(response => {

        if(response.data.length>0){
            this.setState({commentslist: Object.values(response.data)})
        }
    })
    
}

render(){
    var l = this.state.commentslist
    const map7 = l.map(item =>  <Comment
                                    idComment={item["idComment"]}
                                    idEvent ={item["idEvent"]}
                                    user_name ={item["user_name"]}
                                    date={item["date"]} 
                                    content={item["content"]}  
                                    cle={this.props.cle}
                                    getHome={this.props.getHome}
                                     
                                />
                         ); 

    return (
        <div class="col mb-2 " >
            <div class="card" style={{opacity:'0.9',color:'#5C5939' ,fontWeight:'bold',fontFamily:'\"Times New Roman\", Times, serif'}} >
                 <div class="card-body" style={{backgroundColor:'#D6AE3D'}}>
                <h5 class="card-title"> {this.props.name}</h5>
                <p class="card-text"> {this.state.nbParticipants} person joined to this event . place is {this.props.lieu}
                and date is {this.props.date}. created By: {this.props.owner} </p>
                <Button onClick={()=> this.handleJoinEvent()} color="#7770B2" class="btn btn-outline-success" style={{backgroundColor:'#45484D' }}>Join</Button>
                <Button color="#7270B2" id={"com"+this.props.event_id} style={{ marginBottom: '0rem' , marginLeft: '1rem',backgroundColor:'#45484D' }} class="placeOfBtn">
                comments
                </Button>
                <UncontrolledCollapse toggler={"#com"+this.props.event_id}>
                <Card>
                <CardBody>
                 {map7}
                <textarea id="form7" className="md-textarea form-control" rows="3"  onChange={this.handleInputComment} placeholder="your comment !!!" autocomplete="off" />
                <Button  onClick={()=> this.handleComment()}  color="success" id="toggler" style={{ marginBottom: '1rem',backgroundColor:'#45484D' ,color:'white'}}>         
                      ok
                </Button>
                </CardBody>
            </Card>
            </UncontrolledCollapse>
            </div>
        </div>
        </div>
    )
}
}
export default Event;


        