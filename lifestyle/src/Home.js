  
import React, { Component } from 'react';
import axios from 'axios'
import Event from './Event'
import User from './User'
import './connexion.css'
import './css/my_style.css'
import {Container, Card } from "reactstrap";

/**
 * Composant de la page Home on y affiche les utilisateurs qui sont pas nos amis avec possiblité de les follow
 * et la liste des évènements existant
 * 
 * @author Shokufeh AHMADI SIMABE -- Nassima GHOUT
 */

class Home extends Component {
        constructor(props){
            super(props)
            this.state={listM:[], message: null , listAllEvents: [] ,listUsers: []}
            this.allEvents()
            this.allEvents =this.allEvents.bind(this)
            this.getAllUsers()
            this.getAllUsers=this.getAllUsers.bind(this)
        }
        /**
         * Appel de la servlet pour récupérer tout les évènements
         */
        allEvents(){
          axios.get('http://localhost:8080/Life_Style/ListAllEvents')
          .then(response => {
                if(response.data.length>=0){
                     this.setState({listAllEvents: Object.values(response.data)})
                }
                else {
                  alert("something wrong")
                }
            })   
        }
        /**
         * récupérer la liste des utilisateurs
         */
        getAllUsers(){
         var url= new URLSearchParams();
          url.append('key', this.props.cle)
          axios.get('http://localhost:8080/Life_Style/ListUsers?'+url)
          .then(response => {
              if(response.data.length>0){
              this.setState({listUsers: Object.values(response.data)})
         }
		  	})   
        }    
       
    render(){
      var l = this.state.listAllEvents
      const map3 = l.map(item =>  <Event
                                    user_id={this.props.user_id}
                                    event_id ={item["event_id"]}
                                    owner={item["owner"]}  
                                    name= {item["name"]}
                                    date={item["date"]}
                                    lieu={item["lieu"]}
                                    created_at={item["created_at"]}
                                    cle = {this.props.cle}
                                    getHome ={this.props.getHome}
                                    
                                />
                         ); 
  var l1 = this.state.listUsers
  const map9 = l1.map(item =>  <User
                               user_id={item["user_id"]}
                               login ={item["user_login"]}
                               cle = {this.props.cle}
                               listUsers={this.state.listUsers}
                               setFriends={this.setFriends}
                               getAllUsers={this.getAllUsers}
                                                     />
                                              );                      
    return(
      <Container class="homeContainer"style={{marginRight:'0',marginLeft:'0',marginRight:'0',marginLeft:'0',paddingRight:'0',paddingLeft:'0', maxWidth: '1450px'}} >
        <Card style={{marginRight:'0',marginLeft:'0',width:'100%'}} className="backim">  
            <div class="homeImg" >
              <div class="row2 userShow" style={{marginRight:'0',marginLeft:'0',width:'100%'}}>
                {map9}
              </div>
              <div class="row-cols-md-2">
              {map3}
              </div>
            </div>
          </Card>
      </Container>
    );
}}
export default Home;