import React, { Component } from 'react';
import './css/my_style.css';
import axios from 'axios'
import './connexion.css'
import a from './img/avatar/sh.jpg'

/**
 * Component qui represent tous les users qui sont pas amie
 * 
 * @author Shokufeh AHMADI SIMABE -- Nassima GHOUT
 */

class User extends Component {
    constructor(props){
        super(props)
        this.state={avatar:a}
        this.followUser =this.followUser.bind(this)
    
    }
    /**
     * ajout un user dans le liste d'amie
     */
followUser(){

    var code
    var url= new URLSearchParams();
    url.append('key', this.props.cle)
    url.append('id_friend',this.props.user_id)
    axios.get('http://localhost:8080/Life_Style/AddFriend?'+url)
			.then(response => {
                code=response.data["code"];
				if(code === 200){
             alert("Add Successful")
                }
				else if(code === 2){
					alert("vous etes deja ami")
				}
				else if(code === 1000){
					alert("User not Exist")
				}
			})   
}
render(){

return(
    <div class="col">
    <div class="card2 circle2" style={{ borderRadius: '50%' ,textAlign:'center',marginLeft:'20px',marginRight:'20px',opacity:'0.8'}}>
      <div class="card-body ">
          <div class="text-center imgCont">
        
          </div>
        <h6 class="card-title">{this.props.login}</h6>
        <button type="button" class="btn btn-outline-success" onClick={()=> this.followUser()} >Follow</button>
      </div>
    </div>
  </div>
)

    
}
}
export default User;


    