import React, { Component } from 'react';

import {NavItem} from 'reactstrap';
import axios from 'axios'
import Friends from './Friends';
import Profile from './Profile';
import CreateEvent from './CreateEvent';
import Exercises from './Exercises';
import Home from './Home';
import Cloud from './img/meteo/cloud.jpg';
import LightRain from "./img/meteo/RainCloudSun.jpg";
import Rain from "./img/meteo/rain.jpg";
import Snow from "./img/meteo/snow.jpg";
import Eclaire from "./img/meteo/eclair.jpg";
import Sunny from "./img/meteo/suuny.jpg";
import Windy from "./img/meteo/wind.jpg";
import UserSample from "./img/avatar/usr.png"
import './css/my_style.css'
import './css/Accueil.css'
import './js/myJavaScript'
import { Avatar } from 'antd';
/**
 * Classe représesentant le composant Accueil, qui est le composant principal
 * il permet la transition entre page, au sens où il partage l'ecran en 2
 * une partiepermanente où se trouve les bouttons permettant de changer de page
 * et le contenu qui change selon la page choisi
 * Une fois connecté on est amené à la Page Home
 * 
 * @author Shokufeh AHMADI SIMABE -- Nassima GHOUT
 *
 */


class Accueil extends Component {
    constructor(props){
      super(props)
      this.state={ListUsers:[], listFriends:[], user_id :null , tempMax:null, tempMin:null , typeWeath :null ,lat: null , lng:null,description:null ,imge:Sunny, userimg:UserSample}
      this.handleCheckDate=this.handleCheckDate.bind(this)
      this.handleCheckDate()
      this.Users=this.Users.bind(this)
      this.getMyFriends=this.getMyFriends.bind(this)
      this.setListFriends=this.setListFriends.bind(this)
      this.setId= this.setId.bind(this)
      this.handleHome = this.handleHome.bind(this)
      this.handleProfile = this.handleProfile.bind(this)
      this.handleFriends = this.handleFriends.bind(this)
      this.handleAccueil = this.handleAccueil.bind(this)
      this.handleCreateEvent = this.handleCreateEvent.bind(this) 
      this.handleExercises = this.handleExercises.bind(this)//here
      this.handleClick= this.handleClick.bind(this)
      this.weather= this.weather.bind(this)
      this.setImg = this.setImg.bind(this);
      this.weather()

    }
    /**
     * permet de changer la photo de profil
     * (non fonctionnel pour le moment)
     * @param {*} value 
     */
    setImg(value){
      this.setState({img: value });
    }
    /**
     * appeler la servlet weather pour récupérer la météo, et la latitude et langetude selon l'adresse
     * de l'utilisateur, à fin de retrouver les parcs à proximité
     */
    weather(){
      var code
      var url= new URLSearchParams(); 
    url.append('key',this.props.cle)
    axios.get('http://localhost:8080/Life_Style/ShowWeather?'+url)
    .then(response => {
      code=response.data["code"];
      if(code === 200){
                      this.setState({tempMax:response.data['tempMax'] ,tempMin: response.data['tempMin'],typeWeath: response.data['typeWeath'],
                      lat: response.data['lat'],lng: response.data['lng'] , description: response.data['description']})
                  if(this.state.description === "light rain"){
                      this.setState({imge : LightRain})
                  } 
                  if(this.state.description === "rain" || this.state.description === "rainy"){
                      this.setState({imge : LightRain})
                  } 
                  else if(this.state.description === "Sunny" || this.state.description === "sunny" || this.state.description === "sun") 
                  {
                      this.setState({imge : Sunny})
                  }    
                  else if(this.state.description === "snow" || this.state.description==="light snow" || this.state.description==="rain and drizzle") 
                  {
                      this.setState({imge : Snow})
                  }   
                  else if(this.state.description === "Cloud" || this.state.description === "light cloud" || this.state.description==="few clouds" ||this.state.description ==="overcast clouds"||this.state.description==="broken clouds"||this.state.description==="scattered cloud") 
                  {
                      this.setState({imge : Cloud})
                  }   
                  else if(this.state.description === "eclaire") 
                  {
                      this.setState({imge : Eclaire})
                  }   
                  else if(this.state.description === "Windy" || this.state.description === "wind") 
                  {
                      this.setState({imge : Windy})
                  }   
              }
       
      else if(code === 1 ||code===-1){
        alert("something wrong")
      }
    })   
  }
    /**
     * changer l'Id du user
     * @param {*} tmp 
     */
    setId(tmp){
      this.setState({user_id:tmp})
    }
    /**
     * Gestion de Accueil
     */
    handleAccueil(){
        this.props.getAccueil()
    }
    /**
     * Gestion de l'appuie sur le boutton Home
     */
    handleHome(){
      this.props.getHome()
    }
    /**
     * Gestion du boutton Friends
     */
    handleFriends(){
        console.log("Go to Friends")
        this.props.getFriends()
    }
    /**
     * Allerr au profil
     * @param {*} value 
     */
    handleProfile(value){
        this.props.getProfile(value)
    }
    
    handleUsers(){
        this.props.getUsers()
    }
    /**
     * aller à la page évènemnt pour en créer de nouveaux
     */
    handleCreateEvent(){  
        this.props.getCreateEvent()
    }
    /**
     * aller à la page exercices et voir les exercices suggéré du jour via l'api
     */
    handleExercises(){ //Here
        this.props.getExercises();
    }

    setListFriends(tmp){
      this.setState({listFriends:tmp})
    }
    /**
     * appel de la servlet CheckDate 
     * permettant de retirer de la liste les évènements qui sont passé (appelé à chaque connexion)
     * 
     */
    handleCheckDate(){
      var code
      var url= new URLSearchParams(); 
      url.append('key',this.props.cle)
       axios.get('http://localhost:8080/Life_Style/CheckDates?'+url)
        .then(response => {
          code=response.data["code"];
          console.log(code)
        })
    }
    /**
     * Gestion de la déconnexion : appel servlet logout
     * 
     */
    handleClick(){
      var code
      var url= new URLSearchParams(); 
      url.append('key',this.props.cle)
      
      axios.get('http://localhost:8080/Life_Style/Logout?'+url)
        .then(response => {
            code=response.data["code"]
          if(code === 200){
            this.props.setLogout()
            this.props.getInscrit()
          }
          else if(code === 10){
            alert("Cle invalide")
          }
          
          else if(code === 100){
            alert("cle inexistante")
        }
        })   
        
    }
    /**
     * Appel de la servlet listMyFriends pour récupérer ma ilste d'amis
     */
    getMyFriends(){
      var url= new URLSearchParams();
   
      url.append('login',this.props.login)
      axios.get('http://localhost:8080/Life_Style/ListFriends?'+url)
        .then(response => {
            if(typeof response.data != 'undefined'){
             var code = response.status
              if(code === 200){
              this.setState({listFriends: response.data["login"]})

               }
               else if(code === -1){
                 alert("Pas d'arguments")
              }
                else if(code === 1){
                  alert("User not exists")
             }
          
        }
        else{
          console.log("you have no friends")
        }
        }
       
        );  
  }
  /**
   * afficher la liste de tout les utilisateurs
   * 
   */
    Users(){
      var code
      var url= new URLSearchParams();
      if(this.props.login!==null){
                 
      url.append('login',this.props.login)
      axios.get('http://localhost:8080/Life_Style/GetUsers?'+url)
        .then(response => {
          if(typeof response.data != 'undefined'){

            code=response.data["code"];
          if(code[0] === 200){
              this.setState({ListUsers: response.data["login"]})
  
          }
          else if(code === -1){
            alert("Pas d'arguments")
          }
         
        }});  
      }
      
  }
  /**
   * l'affichage de la page
   * selon la page séléctionné par l'utilisateur on retourne le composant correspondant
   * en passant par props tout les attributs adéquants
   */

    render() {
      var page
      
      if(this.props.page ==="Profile"){
       
        page = <Profile cle={this.props.cle}
                        setKey={this.props.setKey}
                        getLogin = {this.props.getLogin}
                        setLogin = {this.props.setLogin}
                        login={this.props.login}
                        setId={this.setId}
                        user_id={this.state.user_id}
                        lat={this.state.lat}
                        lng={this.state.lng}
                        tempMax={this.state.tempMax}
                        tempMin={this.state.tempMin}
                        img={this.state.img}
                        setImg={this.setImg}
                          />

      }
      else if(this.props.page ==="CreateEvent"){
        page = <CreateEvent cle={this.props.cle}
                          setKey={this.props.setKey}
                          getLogin = {this.props.getLogin}
                          setLogin = {this.props.setLogin}
                          login={this.props.login}
                          setId={this.setId}
                          user_id={this.state.user_id}
                          getHome = {this.props.getHome}
                          getProfile = {this.props.getProfile}
                          getCreateEvent ={this.props.getCreateEvent}
                          lat = {this.state.lat}
                          lng={this.state.lng}               
                         
          /> 

      }
      else if(this.props.page ==="Exercises"){
        page = <Exercises cle={this.props.cle}
                          setKey={this.props.setKey}
                          getLogin = {this.props.getLogin}
                          setLogin = {this.props.setLogin}
                          login={this.props.login}
                          setId={this.setId}
                          user_id={this.state.user_id}
                          getHome = {this.props.getHome}
                          getProfile = {this.props.getProfile}
          /> 

      }
      
      else if(this.props.page ==="Home"){

        page=<Home  cle= {this.props.cle} 
                    login={this.props.login}
                    setId={this.setId}
                    user_id={this.state.user_id}
                    getHome={this.props.getHome}
                    />
            
      }
      else if(this.props.page ==="Friends"){
        console.log("Friends ControlePannel")
        page=<Friends cle= {this.props.cle} 
                      login={this.props.login}
                      getUserLogin={this.getUserLogin}
                      getUserProfile={this.props.getUserProfile}
                      setId={this.setId}
                      page = {this.props.page}
                      getFriends ={this.props.getFriends}

                    //user_id={this.state.user_id}
                    />
      }  
      return(
      
          <div className="NavBar" key="Accueil">
          <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <NavItem className="navbar-brand" >LifeStyle</NavItem>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                
                <ul className="navbar-nav mr-auto">
                    
                    <li className="nav-item">
                        <button  onClick={()=>this.handleHome()} className="btn btn-light">Home</button>
                    </li>
 
                    <li className="nav-item">
                        <button onClick={()=>this.handleProfile()} className="btn btn-light" >Profile</button>
                    </li>
                    <li className="nav-item">
                        <button onClick={()=>this.handleFriends()} className="btn btn-light" >Friends</button>
                    </li>
                  
                    <li className="nav-item">
                        <button  onClick={()=>this.handleCreateEvent()} className="btn btn-light">New Events</button>
                    </li>
                    <li className="nav-item">
                        <button  onClick={()=>this.handleExercises()} className="btn btn-light">Exercises</button>
                    </li>
                    <li className="nav-item">
                        <button  onClick={()=>this.handleClick()} className="btn btn-outline-danger">Sign out</button>
                    </li>
        
                </ul>
            </div>
         </nav>
         <div class="homeImg">
            <div class="card-group"  style={{ textAlign: 'center'}}>
                <div class="card " style={{ marginRight:'0'}}>
                     <img src={this.state.userimg} class="card-img-top fixSize" style={{ height: '80px' ,width:'80px' ,marginLeft:'150px' , marginTop:'50px'}}></img>
                </div>
                <div class="card " style={{ marginLeft:'0' , marginRight:'0' , marginTop:'50px'}}>
                    <h3>Life-Style</h3>
                    <div class="card-text">Try LifeStyle And Change your Life Stryle!</div>
                </div>
                <div class="card " style={{ marginLeft:'0'}}>
                  <img src={this.state.imge} class="card-img-top fixSize" style={{ height: '80px' ,width: '80px' , marginLeft:'150px'}}></img>
                  <div class="card-text">Min temp:{this.state.tempMin}</div>
                  <div class="card-text">Max temp:{this.state.tempMax}</div>
                  <div class="card-text">Weather type:{this.state.description}</div>
                  
                </div>
            </div> 
            <div >{page}</div>
         
        </div>
        </div>                        
        );
    }
}
  
  export default Accueil;
