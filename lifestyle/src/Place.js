import React, { Component } from 'react';
import './connexion.css'

/**
 * Component qui represent chaque endroit 
 * @author Shokufeh AHMADI SIMABE -- Nassima GHOUT
 */
class Place extends Component {
    constructor(props){
        super(props)
    }
   
    render(){
        return (
                 <option value={this.props.addr}>{this.props.title},{this.props.addr}</option>
        )
            
    }
    }
    export default Place;
    