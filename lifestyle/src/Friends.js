import React, { Component } from 'react';
import './css/bootstrap.min.css'
import Friend from './Friend'
import axios from 'axios'

/**
 * Component de la page d' ami
 * @author Shokufeh AHMADI SIMABE -- Nassima GHOUT
 */
class Friends extends Component {
    constructor(props) {
        super(props)
        this.state={listFriend:[], avatar:'./img/avatar/sh.jpg'}
        this.AllFriends()
        this.AllFriends =this.AllFriends.bind(this)
    }
    AllFriends(){
        var url= new URLSearchParams();
        url.append('key',this.props.cle)
		axios.get('http://localhost:8080/Life_Style/ListFriends?'+url)
			.then(response => {
                console.log("response ", response.data)
                if(response.data.length >0)
                {
                
                        this.setState({listFriend: Object.values(response.data)})
                        console.log (this.state.listFriend)
                        console.log ("length  ",this.state.listFriend.length)
                }
                else {
                    alert("there is no friends")
                }

			})  
    }
    render() {
        
        var l = this.state.listFriend 
        const map4 = l.map(item =>  <Friend
                            id={item["id_friend"]}
                            date={item["date"]}  
                            login= {item["login"]}
                            cle = {this.props.cle}      
                            getFriends={this.props.getFriends}                      
                                    />
                            ); 
        return(       
            <div className="Containers card fback" style={{marginLeft:'0px',marginRight:'0px',backgroundSize:'contain'}}>
                {map4}
            </div>
        )
     
            
    }

}
export default Friends;
