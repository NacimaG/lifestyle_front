import React, { Component } from 'react';
import axios from 'axios'
import './connexion.css'
import UserImg from './img/avatar/usr.png'
import {Button} from "reactstrap";

/**
 * Composant représentant un ami
 * 
 * @author Shokufeh AHMADI SIMABE -- Nassima GHOUT
 */
class Friend extends Component {
        constructor(props){
            super(props)
            this.state={avatar:UserImg}
            this.handlUnFriend = this.handleUnFriend.bind(this)
            
        }
    
        /**
         * gestion de 
         */
handleUnFriend(){
    var code
    var url= new URLSearchParams();
    url.append('key', this.props.cle)
    url.append('id_friend',this.props.id)
    axios.get('http://localhost:8080/Life_Style/RemoveFriend?'+url)
			.then(response => {
				code=response.data["code"];
				if(code === 200){
               alert("unfriend successful")
         
                }
				else if(code === -1){
					alert("Pas d'arguments")
				}
				else if(code === 1){
					alert("User not connected")
				}
			})   
}    
handleImg(){
    this.setState({avatar : 'img/avatar/sh.jpg'})
}    
render(){
    return (

  <div class="card mb-3 sizeBox" style={{backgroundColor:"#FAEBD7", opacity:'0.8',color:'black',marginLeft:'auto',marginRight:'auto'}}>
  <div class="row no-gutters">
    <div class="col-md-4">
      <img src={this.state.avatar} class="card-img" alt="..." />
    </div>
    <div class="col-md-8">
      <div class="card-body">
        <h5 class="card-title" > User : {this.props.login}</h5>
         <p class="card-text">My name is {this.props.name} we are friends since {this.props.date} </p>

         <Button   onClick={() => this.handleUnFriend()}   color="primary" id="toggler" style={{ marginBottom: '1rem' }}> UnFriend</Button>

      </div>
    </div>
  </div>
</div>
   )
    
    
    }
    }
    export default Friend;
    