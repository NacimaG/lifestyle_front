import React, { Component } from 'react';
import './css/bootstrap.min.css'
import './css/my_style.css'
import axios from 'axios'
import {Cloud, LightRain,Rain, Snow, Eclaire, Sunny, Windy} from './img/meteo/cloud.jpg';
import LoginPage from './img/login.PNG'
import { Card, CardHeader,CardBody,   Button, UncontrolledCollapse,   Container, Row,  Col  } from "reactstrap";
import EventProfile from './EventProfile';
import JointEventProfile from './JointEventProfile';
import ImageCrop from './imageCrop';
import { IconMap } from 'antd/lib/result';

/**
 *  Component de page profile
 * @author Shokufeh AHMADI SIMABE -- Nassima GHOUT
 */
class Profile extends Component {
    constructor(props) {
        super(props)
        //il faut la definir ici?oui
        this.state={user_id:null,nom:null, prenom:null, sexe:null, age:null, mail:null, phoneNumber:null, poids:null , taille:null,bmi:null,adress:null,
             ZipCode:null ,country:null, listEvent: [],listJointEvent:[] ,imge :Cloud ,loginImg :LoginPage ,
              selectedFile: null, FriendNum:"0" ,CeatedEvent:"0",JointEvent:"0", MyAnalys: null, Status:null,description2:null, edit:null, scaleValue:1}
        this.profil = this.profil.bind(this)
        this.EventList =this.EventList.bind(this)
        this.JointEventList =this.JointEventList.bind(this)
        this.profil()
        this.EventList()
        this.JointEventList()
        this.CountFriends()
        this.CountFriends=this.CountFriends.bind(this)
        this.CountCreatedEvent()
        this.CountCreatedEvent=this.CountCreatedEvent.bind(this)
        this.CountJointEvent=this.CountJointEvent.bind(this)
        this.updateInputPassword = this.updateInputPassword.bind(this)
        this.updateInputNom = this.updateInputNom.bind(this)
        this.updateInputPrenom = this.updateInputPrenom.bind(this)
        this.updateInputSexe = this.updateInputSexe.bind(this)
        this.updateInputAge = this.updateInputAge.bind(this)
        this.updateInputMail = this.updateInputMail.bind(this)
        this.updateInputPhone = this.updateInputPhone.bind(this)
        this.updateInputPoids = this.updateInputPoids.bind(this)
        this.updateInputTaille = this.updateInputTaille.bind(this)
        this.updateInputAdresse = this.updateInputAdresse.bind(this)
        this.updateInputZipCode = this.updateInputZipCode.bind(this)
        this.confirmPwd = this.confirmPwd.bind(this)
        this.updateInputCountry = this.updateInputCountry.bind(this)
        this.AnalaysisPoid= this.AnalaysisPoid.bind(this)
        this.AnalaysisPoid()
        this.CountJointEvent()
        this.onClose = this.onClose.bind(this)
        this.setEditorRef = this.setEditorRef.bind(this)
        this.onCrop= this.onCrop.bind(this)
        this.DataURLtoFile=this.DataURLtoFile.bind(this)
        this.profilePicChange =this.profilePicChange.bind(this)
      }

  // ##################################

    setEditorRef = editor => this.setState({ editor});
    onCrop = () => {
      const { editor } = this.state;
      if (editor !== null) {
        const url = editor.getImageScaledToCanvas().toDataURL();
        this.props.setImg(url);
      }
    };
    onScaleChange = (scaleChangeEvent) => {
      const scaleValue =  parseFloat(scaleChangeEvent.target.value);
      this.setState({ scaleValue });
    };

    DataURLtoFile = (dataurl, filename) => {
      let arr = dataurl.split(','),
        mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]),
        n = bstr.length,
        u8arr = new Uint8Array(n);
      while (n--) {
        u8arr[n] = bstr.charCodeAt(n);
      }
      return new File([u8arr], filename, { type: mime });
    };
    
     profilePicChange = (fileChangeEvent) => {
        const file = fileChangeEvent.target.files[0];
        const { type } = file;
        if (!(type.endsWith('jpeg') || type.endsWith('png') || type.endsWith('jpg') || type.endsWith('gif'))) {
        } else {
          this.setState({ openCropper: true, selectedImage: fileChangeEvent.target.files[0], fileUploadErrors: [] });
        }
      };

  //###############################################
  updateInputNom = event => {
      this.setState({ nom: event.target.value })
  }
  updateInputPrenom = event => {
      this.setState({ prenom: event.target.value })
  }
  updateInputMail = event => {
      this.setState({ mail: event.target.value })
  }
  updateInputPassword = event => {
      this.setState({ password: event.target.value })
  }
  confirmPwd = event => {
      this.setState({ password2: event.target.value })
  }
  updateInputPhone = event => {
      this.setState({ phoneNumber: event.target.value })
  }
  updateInputSexe = event => {
      this.setState({ sexe: event.target.value })
  }
  updateInputAge = event => {
      this.setState({ age: event.target.value })
  }
  updateInputPoids = event => {
      this.setState({ poids: event.target.value })
  }
  updateInputTaille = event => {
      this.setState({ taille: event.target.value })
  }
  updateInputAdresse = event => {
      this.setState({ adress: event.target.value })
  }
  updateInputZipCode = event => {
      this.setState({ ZipCode: event.target.value })
  }
  updateInputCountry = event => {
      this.setState({ country:event.target.value})
  }
  /**
   * Analysis de BMI
   */
  AnalaysisPoid(){
      var code 
      var url= new URLSearchParams();
      url.append('key',this.props.cle)
      axios.get('http://localhost:8080/Life_Style/BmiAnalysis?'+url)
			.then(response => {
                console.log("RESPONSE DATA ::", response.data)
                this.setState({ Status:response.data['Status'] })
                this.setState({ description2:response.data['description'] })
			})   
                console.log("MYANLYSYS::",this.state.MyAnalys)
  }
  /**
   * récupère nombre des amies
   */
    CountFriends(){
		var url= new URLSearchParams();
        url.append('key',this.props.cle)
		axios.get('http://localhost:8080/Life_Style/FriendCount?'+url)
			.then(response => {
			    this.setState({ FriendNum:response.data['FriendNum'] })
      
			
			})   

    }
    /**
   * récupère nombre des Event creer par lui
   */
    CountCreatedEvent(){
		var url= new URLSearchParams();
        url.append('key',this.props.cle)
		axios.get('http://localhost:8080/Life_Style/CountCeatedEvent?'+url)
			.then(response => {    
						this.setState({ CeatedEvent:response.data['CeatedEvent'] })
			})   

    }
  /**
   * récupère nombre des Event qu'il est joints
   */
    CountJointEvent(){
        var url= new URLSearchParams();
        url.append('key',this.props.cle)
            axios.get('http://localhost:8080/Life_Style/JointCount?'+url)
			      .then(response => {
                console.log(response.data)       
				this.setState({ JointEvent:response.data['JointEvent'] })
			})   
    }
    onClose() {
        this.setState({preview: null})
      }
      
      onCrop(preview) {
        this.setState({preview})
      }
  
    /**
     * modifie le coordonne
     */
    handleEdit(){
        var url= new URLSearchParams();
        url.append('key',this.props.cle)
        url.append('nom', this.state.nom)
        url.append('prenom', this.state.prenom)
        url.append('sexe', this.state.sexe)
        url.append('age', this.state.age)
        url.append('mail', this.state.mail)
        url.append('phone', this.state.phoneNumber)
        url.append('poids', this.state.poids)
        url.append('taille', this.state.taille)
        url.append('adress', this.state.adress)
        url.append('ZipCode', this.state.ZipCode)
        url.append('country', this.state.country)
        axios.get('http://localhost:8080/Life_Style/UpdateProfile?'+url)
			.then(response => {
                this.profil();
			})   
    }
  /**
   *   
   * récupère les coordonee de user
   */
   
    profil(){
		var code
		var url= new URLSearchParams();
        url.append('key',this.props.cle)
		axios.get('http://localhost:8080/Life_Style/ShowProfil?'+url)
			.then(response => {
				code=response.data["code"];
				if(code === 200){
						this.setState({user_id:response.data['user_id'],nom: response.data['nom'],prenom: response.data['prenom'],sexe: response.data['sexe'],
                        mail: response.data['mail'],phoneNumber: response.data['phoneNumber'],poids:response.data['poids'],taille:response.data['taille'] 
                            , bmi:response.data['bmi'],adress:response.data['adress'] ,ZipCode:response.data['ZipCode'], country : response.data['Country'], age:response.data['age'] })
          
				}
				else if(code === -1){
					alert("Pas d'arguments **")
				}
				else if(code === 1){
					alert("User not connected")
				}
			})   

    }
    /**
   * récupère list des Event que user a creer
   */
    EventList(){
        var url= new URLSearchParams();
    url.append('key',this.props.cle)
    console.log("key ::", this.props.cle)
		axios.get('http://localhost:8080/Life_Style/ListMyEvents?'+url)
			.then(response => {
                console.log("response ", response.data)
                if(response.data.length >=0)
                {
                
                        this.setState({listEvent: Object.values(response.data)})
                        console.log (this.state.listEvent)
                        console.log ("length  ",this.state.listEvent.length)
                }
                else {
                    alert("Pas d'arguments !!")
                }

			})  

    }
    /**
     * list des event qu'il a joint
     */
    JointEventList(){
      var url= new URLSearchParams();
       url.append('key',this.props.cle)
      console.log('http://localhost:8080/Life_Style/JoinEventList?',url)
       axios.get('http://localhost:8080/Life_Style/JoinEventList?'+url)
      .then(response => {
              console.log("response ", response.data)
              if(response.data.length >=0)
              {
                      this.setState({listJointEvent: Object.values(response.data)})
                      console.log (this.state.listJointEvent)
                      console.log ("length  ",this.state.listJointEvent.length)
              }
              else {
                  alert("Pas d'arguments !!")
              }

    })  

  }

    onChangeHandler=event=>{

        this.setState({
            selectedFile: event.target.files[0],
            loaded: 0,
          })
    
    }
    handleFriends(){
     this.props.getFriends(); //go to page friends
    }
    
    render() {
        
        var l = this.state.listEvent 
        const map2 = l.map(item =>  <EventProfile
                            id={item["id"]} 
                            event_id ={item["event_id"]}
                            user_id={item["user_id"]} 
                            owner={item["owner"]}  
                            name= {item["name"]}
                            date={item["date"]}
                            lieu={item["lieu"]}
                            created_at={item["created_at"]}
                            login = {this.props.login}
                            cle = {this.props.cle}
                                    />
                             )
        var g = this.state.listJointEvent 
        const map99 = g.map(item =>  <JointEventProfile
                              id={item["id"]} 
                              event_id ={item["event_id"]}
                              user_id={item["user_id"]} 
                              owner={item["owner"]}  
                              name= {item["name"]}
                              date={item["date"]}
                              lieu={item["lieu"]}
                              created_at={item["created_at"]}
                              login = {this.props.login}
                              cle = {this.props.cle}
                                      />
                               )                     

                   
        return(
          
      
          
      <Card style={{marginLeft:'0',marginRight:'0',paddingLeft:'0',paddingRight:'0'}}>  
          <Col className="order-xl-2 mb-5 mb-xl-0">
            <Card style={{marginLeft:'0',marginRight:'0',paddingLeft:'0',paddingRight:'0'}} className="backImg">
              <CardBody className="pt-0 pt-md-4"  >
                <Row style={{marginLeft:'350px',marginRight:'350px' , opacity:'0.8'}}>
                  <div className="col" >
                    <div className="card-profile-stats d-flex justify-content-center mt-md-5">
                      <div className="col numPlace"style={{backgroundColor:'#E87C5E', textAlign:'center',borderRadius:'50%',minHeight:'100px' }}>
                        <div className="heading " style={{marginTop:'15px'}}>{this.state.FriendNum}</div>
                        <div className="description">Friends </div>
                      </div>
                      <div className="col numPlace" style={{backgroundColor:'#E87C5E', textAlign:'center',borderRadius:'50%' }}>
                        <div className="heading "style={{marginTop:'15px'}}>{this.state.CeatedEvent}</div>
                        <div className="description">CreateEvent </div>
                      </div>
                      <div className="col numPlace" style={{backgroundColor:'#E87C5E', textAlign:'center',borderRadius:'50%' }}>
                        <div className="heading "style={{marginTop:'15px'}}>{this.state.JointEvent}</div>
                        <div className="description">JointEvent </div>
                      </div>
                    </div>
                  </div>
                </Row>
                <Row style={{backgroundColor:"#A3CBE1",textAlign:'center' ,marginLeft:'auto',marginRight:'auto' , opacity:'0.8'}}>
                    
                   <div className="text-center" style={{ marginLeft:'auto',marginRight:'auto',textAlign:'center',fontFamily:'\"Times New Roman\", Times, serif',color:'#23305B'}}>
                   
                    <div className="h5 font-weight-300" style={{marginTop:'50px',fontWeight:'bold'}}>
              
                      My name is {this.state.prenom}{this.state.nom} 
                    </div>
                    <div className="h5 mt-4">
                      <i className="ni business_briefcase-24 mr-2" />
                      <p>{this.state.mail}</p>
                      <p>{this.state.phoneNumber}</p>
                    <div>
                    <Button  id="toggler1" style={{ marginBottom: '1rem' ,backgroundColor:'#23305B',borderColor:'#23305B',color:'white'}}> Edit profile </Button>
                      <UncontrolledCollapse toggler="#toggler1">
                      <Card style={{backgroundColor:'#EF6B6D'}}>
                          <CardBody >
                          <div className="form-row">
                            <div className="form-group col">
                                <input type="text" onChange={this.updateInputNom} className="form-control" placeholder={this.state.nom} />
                            </div>
                            < div className="form-group col">
                                <input type="text" className="form-control" onChange={this.updateInputPrenom} placeholder={this.state.prenom} />
                            </div>
                        </div>
                        <div className="form-row">
                            <div className="form-group col">
                                <input type="password" className="form-control" onChange={this.updateInputPassword} placeholder="Password *" />
                            </div>
                            <div className="form-group col">
                                <input type="password2" className="form-control" onChange={this.confirmPwd} placeholder="Confrm Password *" />
                            </div>
                        </div>
                        <div className="form-group">
                            <input type="email" className="form-control" onChange={this.updateInputMail} placeholder={this.state.mail} />
                        </div>
                        <div className="form-row">
                            <div className="form-group col">
                                <input type="poids" id="poids" name="text" className="form-control" onChange={this.updateInputPoids} placeholder={this.state.poids} />
                            </div>
                            <div className="form-group col">
                                <input type="taille" id="taille" name="text" className="form-control" onChange={this.updateInputTaille} placeholder={this.state.taille} />
                            </div>
                        </div>
                        <div className="form-row">
                            <div className="form-group col">
                                <select type="sexe" id="sexe" name="text" className="form-control"   value={this.state.selectValue} onChange={this.updateInputSexe}  value = {this.state.sexe}>
                                    <option value="null">choisit entre les deux </option>
                                    <option value="F">Woman</option>
                                    <option value="M">Man</option>
                                </select>
                            </div>
                            <div className="form-group col">
                                <input type="age" id="age" name="text" className="form-control" onChange={this.updateInputAge} placeholder={this.state.age} />
                            </div>
                        </div>
                       <div className="form-group">
                            <input type="tel" id="adresse" name="text" className="form-control" onChange={this.updateInputPhone} placeholder={this.state.phoneNumber} />
                        </div>
                        <div className="form-group">
                            <input type="phone" id="adress" name="text" className="form-control" onChange= {this.updateInputAdresse} placeholder={this.state.adress}  />
                        </div>
                        <div className="form-row">
                            <div className="form-group col">
                                <input type="zipcode" id="zipcode" name="text" className="form-control" onChange={this.updateInputZipCode} placeholder={this.state.ZipCode} />
                            </div>       
                            <div className="form-group col">
                                <input type="country" id="country" name="text" className="form-control" onChange={this.updateInputCountry} placeholder={this.state.country} />
                            </div> 
                        </div>   
                       
                          <Button   onClick={()=> this.handleEdit()}    id="toggler" style={{ marginBottom: '1rem' ,color:'#23305B'}}>
                                      Edit
                                      
                          </Button>
                          
                          </CardBody>
                      </Card>
                      </UncontrolledCollapse>
                      <Button id="toggler2" style={{ marginBottom: '1rem' ,backgroundColor:'#23305B',borderColor:'#23305B' ,color:'white'}}>Weight Analysis </Button>
                      <UncontrolledCollapse toggler="#toggler2">
                        <Card style={{backgroundColor:'#EF6B6D',color:'#23305B'}}>
                            <CardBody style={{fontFamily:'\"Times New Roman\", Times, serif'}}>
                                 <p> your bmi is: {this.state.bmi}<i class="far fa-trash-alt"></i></p>
                                <div>{this.state.Status} </div>
                                <div>{this.state.description2} </div>
                            </CardBody>
                        </Card>
                      </UncontrolledCollapse>
                      <Button  id="toggler3" style={{ marginBottom: '1rem' ,backgroundColor:'#23305B',borderColor:'#23305B',color:'white'}}> New avatar</Button>
                      <UncontrolledCollapse toggler="#toggler3">
                        <Card style={{backgroundColor:'#EF6B6D'}}>
                            <CardBody>
                            <div>
                              <input type="file" name="profilePicBtn" accept="image/png, image/jpeg" onChange={this.profilePicChange} />
                              <ImageCrop
                                imageSrc={this.state.selectedImage}
                                setEditorRef={this.setEditorRef}
                                onCrop={this.onCrop}
                                scaleValue={this.state.scaleValue}
                                onScaleChange={this.onScaleChange}
                              />
   
                              <img src={this.state.userProfilePic} alt="Profile" />
                             </div>
          
                            </CardBody>
                        </Card>
                      </UncontrolledCollapse>  
                  </div>
                  </div>
                </div>
                </Row>
              </CardBody>
              <CardBody className="pt-0 pt-md-4">
                  <div class="row-cols-md-2">
                     {map2}  
                 </div>
              </CardBody>
              <CardBody className="pt-0 pt-md-4">
                  <div class="row-cols-md-2">
                     {map99}  
                 </div>
              </CardBody>
  
            </Card>
          </Col>
     
        </Card>    
        
           
    
        
        );
    }
}
export default Profile;
