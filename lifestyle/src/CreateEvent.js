import React, { Component } from 'react';
import axios from 'axios'
import Place from './Place'
import './css/bootstrap.min.css'
import './connexion.css'
import './Profile'
/**
 * *Creation un event
 * @author Shokufeh AHMADI SIMABE -- Nassima GHOUT
 */

class CreateEvent extends Component {
  constructor(props){
    super(props)
    this.state = { name: null, type: "In", date: null, lieu: null , msg: "" , Parks:null , placelist:[] ,loc:'Parks'}
     // this.handleCancel = this.handleCancel.bind(this)
		//this.weather =this.weather.bind(this);
		this.getPlaces = this.getPlaces.bind(this);
    this.handleCreate = this.handleCreate.bind(this)
    this.getPlaces()
   // this.getPlaces()
  }  

  getPlaces(){
    var code
    var url= new URLSearchParams();
    url.append('lat',this.props.lat)
    url.append('lng',this.props.lng)
    url.append('loc',this.state.loc)
    axios.get('http://localhost:8080/Life_Style/ShowNearParks?'+url)
    .then(response => {
              console.log("reponse ", response.data)
              if(response.data.length>0){
                  this.setState({placelist: Object.values(response.data)})
                  console.log (this.state.placelist)
              }
    })   
  }
	getDate(){
		var date = new Date(),
		todayDate = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
	  this.setState = {today: todayDate};

        }  
	handleCreate(){
    if (this.props.name !== null && this.state.type !== null && this.state.date !== null && this.state.lieu !== null) {
        var code
        var url = new URLSearchParams();
        url.append('key', this.props.cle)
        url.append('name', this.state.name)
        url.append('type', this.state.type)
        url.append('date', this.state.date)
        url.append('lieu', this.state.lieu)
        axios.get('http://localhost:8080/Life_Style/AddEvent?' + url)
            .then(response => {
                console.log("Event Created", response.data);
                code = response.data["code"]
               
                if (code === 200) {
                    this.props.getCreateEvent()
                    alert("add successful")
                    window.location.reload(false);
                  //  this.componentDidMount()
                }
                else if (code === -1) {
                    this.setState({ msg: "Pas d'arguments" })
                    alert(this.state.msg)
                }
                else if (code === -10) {
                    //this.setState({ msg: "event exist deja" })
                    alert("connexion non exist deja")//this.state.msg)
                    //this.props.getCreateEvent()
                }
                else if (code === -2) {
                    this.setState({ msg: "Lieu non renseigné" })
                    alert(this.state.msg)
                }
                
                else if (code === 1) {
                    //this.setState({ msg: "event déjà existant" })
                    alert("exist déjà")//this.state.msg)
                } 
           
            })
    } else {
        alert("champs obligatoire")
    }

  }
   
	updateInputName = event => {
		this.setState({ name: event.target.value })
	}
	updateInputDate= event => {
		this.setState({date :event.target.value})
	}   
	updateInputlieu =  event => {
		this.setState({lieu :event.target.value})
	} 
	updateInputType =   event => {
		this.setState({type :event.target.value})
  } 

	render(){
		
    var l = this.state.placelist 
    const map9 = l.map(item =>  <Place 
                                addr = {item["highlightedVicinity"] }
                                title ={item["title"]}   
                                />
                         ); 
    return (
        <div className="Connexion loginImg" >
            <div className="container">
              <div className="row">
                <div className="col-md-5 mx-auto">
                  <div id="first">
                    <div className="myform form ">
                      <div className="logo mb-3">
                        <div className="col-md-12 text-center">
                          <h1>New Event</h1>
                        </div>
                     </div>
                     <div forname="login">
                     <div className="form-group form-row">
                     <label for="Name" className="col-sm-2 col-form-label col">Name</label>
                          <div className="col-sm-10 col">
                                <input type="text" className="form-control" id="Name" placeholder="Name" onChange={this.updateInputName}/>
                          </div>
                      </div>
                      <div className="form-group form-row">
                      <label for="type1" className="col-sm-2 col-form-label col">type</label>
                      <div className="col-sm-10 col">
                        <select type="type" id="type1" name="text" className="form-control"   value={this.state.selectValue} onChange={this.updateInputType} >
                            <option value="null">choisit entre les deux </option>
                            <option value="In">Inside</option>
                            <option value="Out">OutSide</option>
                        </select>
                        </div>
                      </div>
                      <div className="form-group form row">
                        <label for="Date" className="col-sm-2 col">Date</label>
                        <div className="col-sm-10 col">
                            <input type="date" id="start" name="trip-start" className="form-control" min={this.state.today} max="2021-12-31" onChange={this.updateInputDate} />
                        </div>
                      </div>
                      <div className="form-group form-row">
                          <label for="Lieu" className="col-sm-2 col-form-label col">Place</label>
                          <div className="col-sm-10 col" >
                          <select  className="form-control" id="Lieu" onChange={this.updateInputlieu}>
                          {map9}
                          </select>
                          </div>
                      </div>
                      <div className="col-md-12 text-center ">
                        <button onClick={() => this.handleCreate()} className="btn btn-secondary btn-lg btn-block" type="submit">Let's go</button>
                      </div>
                 </div>

            </div>
            </div>
            </div>
            </div>
            </div>
    
      </div> )}


}       
export default CreateEvent;               