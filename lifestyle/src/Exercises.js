import React, { Component } from 'react';
import axios from 'axios'
import './css/bootstrap.min.css'
import './connexion.css'
import './Profile'
//import Exer from './Exer'
import Activity from './Activity'

class Exercises extends Component {
    constructor(props){
        super(props)
        console.log("in Exercises")
        this.state = {listExer:[]}
        this.getExercisesA()

        this.getExercisesA = this.getExercisesA.bind(this);
    } 
    getExercisesA(){
       console.log("getExercice")
		var url= new URLSearchParams();
        url.append('pageNum',1)
		axios.get('http://localhost:8080/Life_Style/ShowExercise?'+url)
			.then(response => {
                console.log("reppnse ", response.data)
                if(response.data.length>0){
                    this.setState({listExer: Object.values(response.data)})
                    console.log (this.state.listExer)
                }
			})   
    }
    render(){
       
        var l = this.state.listExer 
        const map5 = l.map(item =>  <Activity 
                                    desc = {item["description"] }
                                    name ={item["name_original"]} 
                                    language = {item["language"]}   
                                    />
                             ); 
        return (
            <div className='ExerBack'>
                        <div >{map5}</div>
            </div>
            )
    }
      
  }
  export default Exercises;  
