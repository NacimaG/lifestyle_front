import React, { Component } from 'react';
import { Button } from 'reactstrap';
import Comment from './Comment'
import axios from 'axios'
import './css/my_style.css';
import { UncontrolledCollapse, CardBody, Card } from 'reactstrap';
import './connexion.css'
class JointEventProfile extends Component {
        constructor(props){
            super(props)
            this.state={nbParticipants:"0",newName:this.props.name, newDate:this.props.date, newType:this.props.type, newLieu:this.props.lieu}
            this.handleJoinEvent = this.handleJoinEvent.bind(this) 
            this.Participant();
            this.Participant = this.Participant.bind(this) 
            this.updateInputName = this.updateInputName.bind(this)
            this.updateInputDate =this.updateInputDate.bind(this)
            this.updateInputType =this.updateInputType.bind(this)
            this.updateInputLieu =this.updateInputLieu.bind(this)
            this.handleComment= this.handleComment.bind(this)
            this.handleInputComment=this.handleInputComment.bind(this)
            this.getAllComments();
            this.getAllComments =this.getAllComments.bind(this);
        }
        updateInputName = event => {
            this.setState({ newName: event.target.value })
        } 
        updateInputDate = event =>{
            this.setState({newDate :event.target.value})
        }     
        updateInputType =event => {
            this.setState({newType :event.target.value})
        }
        updateInputLieu =event => {
            this.setState({newLieu :event.target.value})
        }

Participant(){
    var code
    var url= new URLSearchParams();
    url.append('idEvent',this.props.id)
    axios.get('http://localhost:8080/Life_Style/CountPartEvents?'+url)
			.then(response => {
                code=response.data["code"];
				if(code === 200){
                    console.log("NB PARTICIPANT")
                    this.setState({nbParticipants:response.data['nbParticipants']})
                    console.log("PARTS :",this.state.nbParticipants)
                }
				else if(code === -10){
					alert("User not connected false in cle" , this.props.cle)
				}
				else if(code === 1){
					alert("User not connected")
				}
			})
    
}         
handleComment(){
    var code
    var url= new URLSearchParams(); 
    url.append('key', this.props.cle)
    url.append('idEvent',this.props.event_id)
    url.append('content', this.state.content)
    axios.get('http://localhost:8080/Life_Style/NewComment?'+url)
			.then(response => {
               // console.log(response.data)
                code=response.data["code"];
                //alert(code)
				if(code === 200){
                   // alert("Comment successful")
                    this.getAllComments()
                    this.setState({content:"  "})
                    //window.location.reload(false);
                    //this.setState(Friend)
                }
				else if(code === -10){
					alert("User not connected false in cle" , this.props.cle)
				}
				else if(code === 1){
					alert("User not connected")
				}
			})
} 

handleInputComment= (e) =>{
    this.setState({content : e.target.value})
  }

getAllComments(){
    var url= new URLSearchParams(); 
    url.append('idEvent',this.props.event_id)
    axios.get('http://localhost:8080/Life_Style/ListComments?'+url)
    .then(response => {

        if(response.data.length>0){
            this.setState({commentslist: Object.values(response.data)})
        }
    })
}
handleJoinEvent  (){
    var code
    var url= new URLSearchParams(); 
    url.append('name',this.props.name)
    url.append('owner',this.props.owner)
    url.append('key', this.props.cle)
    alert(this.props.cle)
    axios.get('http://localhost:8080/Life_Style/JoinEvent?'+url)
			.then(response => {
                code=response.data["code"];
            
				if(code === 200){
                    this.profil();

                }
				else if(code === -10){
					alert("User not connected false in cle" , this.props.cle)
				}
				else if(code === -1){
					alert("User not connected")
				}
			})
} 
render(){
    return (

        <div class="col mb-2">
            <div class="card" style={{ opacity:'0.8'}}>
                <div class="card-body eventCSS2" style={{backgroundColor:"#A3CBE1"}}>
                <h5 class="card-title"> You Joined to: {this.props.name} </h5>
                <p class="card-text"> {this.state.nbParticipants} other people joined. It will held in <span style={{backgroundColor:'#DB9A7F' , opacity:'0.8'}}>{this.props.lieu}</span> , at: {this.props.date} owner is:{this.props.owner} </p>
                </div>
            </div>
        </div>    
    )  
}
}
export default JointEventProfile;
