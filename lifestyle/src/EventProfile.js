import React, { Component } from 'react';
import { Button } from 'reactstrap';

import axios from 'axios'
import {
    //  Button,
      Card,
      CardBody,
      UncontrolledCollapse,
    } from "reactstrap";
import './connexion.css'
class EventProfile extends Component {
        constructor(props){
            super(props)
            this.state={nbParticipants:"0",newName:this.props.name, newDate:this.props.date, newType:this.props.type, newLieu:this.props.lieu}
            this.handleEditEvent = this.handleEditEvent.bind(this)
            this.handleDeleteEvent = this.handleDeleteEvent.bind(this)
            this.handleJoinEvent = this.handleJoinEvent.bind(this) 
            this.Participant();
            this.Participant = this.Participant.bind(this) 
            this.updateInputName = this.updateInputName.bind(this)
            this.updateInputDate =this.updateInputDate.bind(this)
            this.updateInputType =this.updateInputType.bind(this)
            this.updateInputLieu =this.updateInputLieu.bind(this)
        }
        updateInputName = event => {
            this.setState({ newName: event.target.value })
        } 
        updateInputDate = event =>{
            this.setState({newDate :event.target.value})
        }     
        updateInputType =event => {
            this.setState({newType :event.target.value})
        }
        updateInputLieu =event => {
            this.setState({newLieu :event.target.value})
        }

Participant(){
    var code
    var url= new URLSearchParams();
    url.append('idEvent',this.props.id)
    axios.get('http://localhost:8080/Life_Style/CountPartEvents?'+url)
			.then(response => {
                code=response.data["code"];
				if(code === 200){
                    console.log("NB PARTICIPANT")
                    this.setState({nbParticipants:response.data['nbParticipants']})
                    console.log("PARTS :",this.state.nbParticipants)
                }
				else if(code === -10){
					alert("User not connected false in cle" , this.props.cle)
				}
				else if(code === 1){
					alert("User not connected")
				}
			})
    
}         
handleDeleteEvent() {
    var code
    var url= new URLSearchParams();
    url.append('key', this.props.cle)
    url.append('id',this.props.id)
    axios.get('http://localhost:8080/Life_Style/DeleteEvent?'+url)
			.then(response => {
                code=response.data["code"];
				if(code === 200){
                    alert("Delete Successful")
                    //window.location.reload(false);
                    //this.setState(Friend)
                }
				else if(code === -1){
					alert("Pas d'arguments")
				}
				else if(code === -10){
					alert("User not connected")
				}
			})   
}   
handleEditEvent(){
    var code
    var url = new URLSearchParams();
    url.append('id', this.props.id)
    url.append('key', this.props.cle)
    url.append('name', this.state.newName)
    url.append('type', this.state.newType)
    url.append('date', this.state.newDate)
    url.append('lieu', this.state.newLieu)

    axios.get('http://localhost:8080/Life_Style/EditEvent?'+url)
        .then(response =>{
            code=response.data["code"];
            alert(this.props.cle)
            if(code === 200){
                alert("EditSuccessFul")
            }if(code===-2){
                alert("one field is empty that should'nt")
            }if(code===1){
                alert("you cannot update this event")
            }
        })
}
handleJoinEvent  (){
    var code
    var url= new URLSearchParams(); 
    url.append('name',this.props.name)
    url.append('owner',this.props.owner)
    url.append('key', this.props.cle)
    alert(this.props.cle)
    axios.get('http://localhost:8080/Life_Style/JoinEvent?'+url)
			.then(response => {
                code=response.data["code"];
            
				if(code === 200){
                    this.profil();

                }
				else if(code === -10){
					alert("User not connected false in cle" , this.props.cle)
				}
				else if(code === -1){
					alert("User not connected")
				}
			})
} 
render(){
    return (

        <div class="col mb-2">
            <div class="card" style={{ opacity:'0.8'}}>
                <div class="card-body eventCSS2" style={{backgroundColor:"#A3CBE1"}}>
                <h5 class="card-title"> {this.props.name} </h5>
                <p class="card-text">{this.state.nbParticipants} joined to your events and it will held in <span style={{backgroundColor:'#DB9A7F' , opacity:'0.8'}}>{this.props.lieu}</span> , at: {this.props.date} owner is:{this.props.owner} </p>
                <Button onClick={()=> this.handleDeleteEvent()} style={{backgroundColor:'#23305B'}}>Delete</Button>   
                <Button id="toggler"color="secondary" style={{backgroundColor:'#23305B'}}>Edit </Button> 
                <UncontrolledCollapse toggler="#toggler">
                      <Card>
                          <CardBody>
                          <div className="form-row">
                            <div className="form-group col">
                                <input type="text" onChange={this.updateInputName} className="form-control" placeholder={this.props.name} />
                            </div>
                
                        </div>

                        <div className="form-group">
                            <input type="date" id="start" name="trip-start" className="form-control" min={this.state.today}  max="2021-12-31" onChange={this.updateInputDate} />
                        </div>
                       
                        <div className="form-row">
                            <div className="form-group col">
                                <select type="sexe" id="sexe" name="text" className="form-control"   value={this.props.selectValue} onChange={this.updateInputType} >
                                    <option value="null">choisit entre les deux </option>
                                    <option value="In">In</option>
                                    <option value="Out">Out</option>
                                </select>
                            </div>
                            <div className="form-group col">
                                <input disabled type="lieu" id="lieu" name="text" className="form-control" onChange={this.updateInputLieu} placeholder={this.props.lieu} />{this.props.lieu}
                            </div>
                        </div>
                          <Button   onClick={()=> this.handleEditEvent()}   color="primary" id="toggler" style={{ marginBottom: '1rem' }}>
                                      Edit
                          </Button>
                        
                          </CardBody>
                      </Card>
                      </UncontrolledCollapse> 
                </div>
            </div>
        </div>    
    )  
}
}
export default EventProfile;
